#!/bin/bash
#curl --request GET --header "Private-Token: ${GITLAB_TOKEN_ADMIN}" --header "Content-Type: application/json" https://gitlab.com/api/v4/groups?search=$1

# ./search-group.sh wasm

http GET https://gitlab.com/api/v4/groups?search=$1 "Private-Token":"${GITLAB_TOKEN_ADMIN}" "Content-Type":"application/json" --body --output search.json

jq '.[] | (.id|tostring) + " " + .path + " " + .web_url' --raw-output search.json




