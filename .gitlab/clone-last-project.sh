#!/bin/bash
web_url=$(jq '.web_url' project.json --raw-output)
project_name=$(jq '.name' project.json --raw-output)
echo $web_url
git clone ${web_url}
cd ${project_name}
cp ../../git.sh git.sh
#gp init --interactive
gp init

