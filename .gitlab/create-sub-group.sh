#!/bin/bash

# ./create-sub-group.sh <group_name>

parent_group_id=9964297

http POST https://gitlab.com/api/v4/groups "Private-Token":"${GITLAB_TOKEN_ADMIN}" "Content-Type":"application/json" path="$1" name="$1" parent_id="${parent_group_id}" visibility="public" --body --output group.json
  
jq '(.id|tostring) + " " + .web_url' --raw-output group.json
