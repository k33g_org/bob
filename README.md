# Bob (public)

My **public** Gitpod computer in the cloud

- "Computer as Code", I can restart with a clean computer, when I want
- When I need to work on a specific topic (new language for example), I can create a new branch of my remote computer

"We Are Legion (We Are Bob)" 😉
